﻿using Commander.NET.Attributes;

namespace Litera.Cli.Commander
{
    // Represents the "core" CLI commands 
    internal class LiteraCli
    {
        [Command("report")]
        public GenerateReportCli Report { get; set; }
    }
}
