﻿using Commander.NET.Interfaces;
using Litera.Models.Dtos.Exporting;
using Litera.Models.Enums;
using Litera.Services.Exporting;
using Litera.Services.Reporting;

namespace Litera.Cli.Commander
{
    // Represents the action upon executing the reporting CLI command
    // and the arguments that could be passed to that command
    internal class GenerateReportCli : ICommand
    {
        private readonly ReportGenerator _reportGenerator;
        private readonly ExcelExporter _excelExporter;

        // Example parameter
        public InterviewResult InterviewResult { get; set; }

        public GenerateReportCli()
        {
            _reportGenerator = new ReportGenerator();
            _excelExporter = new ExcelExporter();
        }

        // Represents what to execute when providing the report command from the CLI
        public void Execute(object parent)
        {
            // Generate the report using the parameters
            var result = _reportGenerator.Generate(interviewResult: InterviewResult);

            // Map result to export DTO and export
            _excelExporter.Export(result.Select(x => new ExcelExportDto()).ToList(), "");
        }
    }
}
