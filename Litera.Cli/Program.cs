﻿using Commander.NET;
using Litera.Cli.Commander;

var commandParser = new CommanderParser<LiteraCli>();
commandParser.Parse(args);
