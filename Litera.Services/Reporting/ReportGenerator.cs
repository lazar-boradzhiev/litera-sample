﻿using Litera.Models.Dtos.Csv;
using Litera.Models.Dtos.Reporting;
using Litera.Models.Enums;

namespace Litera.Services.Reporting
{
    public class ReportGenerator
    {
        // Pass the needed repositories to the constructor
        public ReportGenerator()
        {

        }

        // Generate the report after pulling the data using the repositories
        public List<ReportDto> Generate(
            DateTime? fromDate = null, 
            DateTime? toDate = null, 
            InterviewResult? interviewResult = null,
            SelectionResult? selectionResult = null,
            Source? source = null,
            Aggregation? aggregation = null)
        {
            return new List<ReportDto>();
        }
    }
}
