﻿using System.Data.SqlClient;

namespace Litera.Data.Repositories
{
    internal class DummyRepository
    {
        private readonly string _connectionString;

        public DummyRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        // Replace object with DTO.
        public object GetById(Guid id)
        {
            using var conn = new SqlConnection(_connectionString);
            var command = conn.CreateCommand();
            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                break;
            }
            throw new NotImplementedException();
        }


        // Replace object with DTO.
        public List<object> Get(int limit, int offset)
        {
            using var conn = new SqlConnection(_connectionString);
            var command = conn.CreateCommand();
            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                break;
            }
            throw new NotImplementedException();
        }


        // Replace object with DTO.
        public void Create(object dto)
        {
            using var conn = new SqlConnection(_connectionString);
            var command = conn.CreateCommand();
            command.ExecuteNonQuery();
            throw new NotImplementedException();
        }


        // Replace object with DTO.
        public void Update(object dto)
        {
            using var conn = new SqlConnection(_connectionString);
            var command = conn.CreateCommand();
            command.ExecuteNonQuery();
            throw new NotImplementedException();
        }


        // Replace object with DTO.
        public void Delete(Guid id)
        {
            using var conn = new SqlConnection(_connectionString);
            var command = conn.CreateCommand();
            command.ExecuteNonQuery();
            throw new NotImplementedException();
        }
    }
}
